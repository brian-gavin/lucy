#! /usr/bin/env python3
import lucy_ast
import lucy_codegen
import lucy_targen
import sys


DEBUG = 0


def f_in(f):
    rv = ''
    for line in f:
        rv += line
    return rv


def std_in():
    rv = ''
    while 1:
        try:
            line = input('lucy > ')
            if not line:
                break
            rv += line + ' '
        except EOFError:
            break
    return rv


def main():
    f_mode = len(sys.argv) == 2
    if f_mode:
        f = open(sys.argv[1])
        s = f_in(f)
    else:
        s = std_in()

    _ast = lucy_ast.gen_ast(s)
    if DEBUG:
        print('AST:', file=sys.stderr)
        print(_ast._debug(), file=sys.stderr)
    icr = lucy_codegen.gen(_ast)
    if DEBUG:
        print('ICR:', file=sys.stderr)
        for _ in icr:
            print(_, file=sys.stderr)
    lucy_targen.gen(icr)
    return 0

if __name__ == '__main__':
    sys.exit(main())
