from enum import Enum
import sys

class WriteTypes(Enum):
    integer = 1
    string = 5


# AST NODE CLASSES
class Node:
    def __init__(self, val, parent=None, children=None):
        self.val = val
        self.parent = parent
        self.children = []
        self.symtab = None
        if children:
            self.children += children
            for child in self.children:
                child.parent = self
            self.leaf = False
        else:
            self.leaf = True

    def gen_quad(self):
        rv = []
        for child in self.children:
            q = child.gen_quad()
            if type(q) == list:
                rv += q
            else:
                rv.append(q)
        return rv

    def rvalue(self):
        raise Exception('Unspecified rvalue generator for ' + str(type(self)))

    def lvalue(self):
        raise Exception('Unspecified lvalue generator for ' + str(type(self)))

    def _debug(self):
        for child in self.children:
            print(child._debug(), file=sys.stderr)
        return str(self)


class AST(Node):
    def __init__(self, val, children):
        super(AST, self).__init__(val, children=children)

    def gen_quad(self):
        rv = Node.gen_quad(self)
        rv.insert(0, Label('main'))
        return rv


class Statement(Node):
    def __init__(self, val, parent, children):
        super(Statement, self).__init__(val=val, parent=parent)


class Write(Node):
    def __init__(self, val, children):
        super(Write, self).__init__(val, children=children)

    def gen_quad(self):
        rv = []
        for child in self.children:
            q = child.gen_quad()
            if type(q) == list:
                rv += q
            else:
                rv.append(q)
        rv.append(Quad(self.val, self.children[0].rvalue(), arg2=None, res=Temp()))

        return rv


class BinOp(Node):
    def __init__(self, e1, e2, op):
        super(BinOp, self).__init__(val=op, children=[e1, e2])


class Identifier(Node):
    def __init__(self, name):
        super(Identifier, self).__init__(name)

    def rvalue(self):
        return self.val

class Constant(Node):
    def __init__(self, val):
        super(Constant, self).__init__(val)

    def rvalue(self):
        if type(self.val) == int:
            return self.val
        return None


class Declaration(Node):
    def __init__(self, type, assign):
        super(Declaration, self).__init__(Symbol(assign.name, type, assign.val))


class Type(Node):
    def __init__(self, val):
        super(Type, self).__init__(val)


class Assign(Node):
    def __init__(self, name, val):
        super(Assign, self).__init__(val=val)
        self.name = name


class Symbol():
    def __init__(self, name, type=None, val=None, parent_block=None):
        self.name = name
        self.type = type
        self.val = val
        self.parent_block = None


class Quad():
    def __init__(self, op, arg1, arg2, res):
        self.op = op
        self.arg1 = arg1
        self.arg2 = arg2
        self.res = res

    def __str__(self):
        return '<QUAD: ' + str(self.op) + ' ' + str(self.arg1) + ' '\
               + str(self.arg2) + ' ' + str(self.res) + '>'

class Temp():
    temp_cnt = 0
    def __init__(self):
        self.num = Temp.temp_cnt
        Temp.temp_cnt += 1


class Label():
    def __init__(self, arg=None):
        if arg:
            self.lbl = LabelGen.gen(arg)
        else:
            self.lbl = LabelGen.gen()

    def __str__(self):
        return self.lbl

class LabelGen():
    label_cnt = 0

    @staticmethod
    def gen(arg=None):
        if arg:
            return arg +':'
        LabelGen.label_cnt += 1
        return 'L' + str(LabelGen.label_cnt) + ':'


class Instruction(Enum):
    ADD = 0
    WRITE_INT = 1
    LOAD_IMMED = 2
    LOAD_WORD = 3
    STORE_WORD = 4
    WRITE_STRING = 5
    LOAD_ADDR = 6


class Register():
    def __init__(self, val):
        self.val = val

    def __eq__(self, other):
        return self.val == other.val

    def __str__(self):
        return str(self.val)