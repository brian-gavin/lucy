from lucy_classes import *
from lucy_codegen import global_symbols
from lucy_codegen import global_string
from ply import yacc
from ply import lex

reserved = {
    'write_int': 'WRITE_INT',
    'write_string': 'WRITE_STRING',
    'main': 'MAIN',
    'int': 'INT',
    'void': 'VOID',
}

tokens = (
    'ID',
    'LPAREN',
    'RPAREN',
    'LBRACE',
    'RBRACE',
    'PLUS',
    'SEMICOLON',
    'NUMBER',
    'EQUAL',
    'STRING',
) + tuple(reserved.values())


# TOKENS
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_LBRACE = r'{'
t_RBRACE = r'}'
t_PLUS = r'\+'
t_SEMICOLON = r';'
t_EQUAL = r'\='
t_STRING = r'\".*?\"'
# ignored
t_ignore = ' \t\r'

# special action tokens
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)


def t_NUMBER(t):
    r'\d+'
    t.value = int(t.value)
    return t


def t_ID(t):
    r'[a-zA-Z_][a-zA-Z0-9_]*'
    t.type = reserved.get(t.value, 'ID')
    return t


# PRODUCTION RULES
def p_program(p):
    """program : decls main_block"""
    global_symbols(p[1])
    p[0] = p[2]


def p_main(p):
    """main_block : VOID MAIN LPAREN RPAREN LBRACE stmts RBRACE"""
    p[0] = AST(None, children=[p[6]])


def p_decls(p):
    """decls : decls decl
             | decl
             | """
    if len(p) == 3:
        p[1].children.append(p[2])
    elif len(p) == 2:
        p[0] = p[1]


def p_decl(p):
    """decl : type ID SEMICOLON"""
    p[0] = Declaration(p[1], Assign(p[2], None))


def p_assign(p):
    """assign : ID EQUAL expr SEMICOLON"""
    p[0] = Assign(p[1], p[2])


def p_type(p):
    """type : INT"""
    p[0] = Type(p[1])


def p_stmts(p):
    """stmts : stmts stmt
             | stmt"""
    if len(p) == 3:
        p[1].children.append(p[2])
    p[0] = p[1]


def p_assign_stmt(p):
    """stmt : assign"""
    p[0] = p[1]


def p_write_int_stmnt(p):
    """stmt : WRITE_INT LPAREN expr RPAREN SEMICOLON"""
    p[0] = Write(WriteTypes.integer.value, children=[p[3]])


def p_write_string_stmt(p):
    """stmt : WRITE_STRING LPAREN str_lit RPAREN SEMICOLON"""
    p[0] = Write(WriteTypes.string.value, children=[p[3]])


def p_str_lit(p):
    """str_lit : STRING """
    lbl = LabelGen.gen()
    global_string(Symbol(lbl, type=str, val=p[1]))
    p[0] = Identifier(lbl)


def p_binop_expr(p):
    """expr : expr PLUS expr"""
    p[0] = BinOp(p[1], p[3], p[2])


def p_id(p):
    """expr : ID"""
    p[0] = Identifier(p[1])


def p_constant(p):
    """expr : NUMBER"""
    p[0] = Constant(p[1])


def p_error(p):
    raise SyntaxError(p)


def gen_ast(input_code):
    lex.lex()
    parser = yacc.yacc()
    root = parser.parse(input_code)
    return root