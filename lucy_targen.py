import sys
from lucy_classes import Register
from lucy_classes import Instruction
from lucy_classes import Label
from lucy_codegen import GLOBAL_SYMTAB

TEXT_SEGMENT = ['.text']
DATA_SEGMENT = ['.data', '.align 3', '__newline: .asciiz "\\n"',]

TYPE_SIZE  = {
    'int': 4,
}

START_PROCEDURE = """
_start:
jal main
li $v0, 10
syscall
"""

MAIN_FINI = """
jr $ra"""

BUILTIN_WRITE_INT_MACRO = """
.macro write_int(%val)
move $a0, %val
li $v0, 1
syscall
la $a0, __newline
li $v0, 4 
syscall
.end_macro
"""

BUILTIN_WRITE_STRING_MACRO = """
.macro write_string(%val)
la $a0, %val
li $v0, 4
syscall
la $a0, __newline
li $v0, 4 
syscall
.end_macro
"""


def builtin_write(reg):
    return 'write_int($'+ str(reg_index(reg)) + ')'


def builtin_write_string(lbl):
    return 'write_string(' + lbl + ')'



INSTRUCTIONS = {
    Instruction.ADD.value: 'add',
    Instruction.WRITE_INT.value: builtin_write,
    Instruction.LOAD_IMMED.value: 'li',
    Instruction.LOAD_WORD.value: 'lw',
    Instruction.STORE_WORD.value: 'sw',
    Instruction.WRITE_STRING.value: builtin_write_string,
    Instruction.LOAD_ADDR.value: 'la',
}


TEXT_SEGMENT.append(BUILTIN_WRITE_INT_MACRO)
TEXT_SEGMENT.append(BUILTIN_WRITE_STRING_MACRO)
TEXT_SEGMENT.append(START_PROCEDURE)

REG_TEMPS = range(8,26)


REGS = { n:Register(None) for n in REG_TEMPS } # general purpose
REGS[0] = Register(0)


def reg_index(reg):
    f =filter(lambda t: t[1] == reg, REGS.items())
    return tuple(f)[0][0]


def get_reg(val):
    empty = Register(None)
    if Register(val) in REGS.values():
        return reg_index(Register(val))
    elif empty in REGS.values():
        index = reg_index(empty)
        REGS[index] = Register(val)
        if is_constant(val):
            TEXT_SEGMENT.append(
                gen_load_instruction(Instruction.LOAD_IMMED.value,
                                     REGS[index],
                                     val))
        elif type(val) == str and ':' in val:
            TEXT_SEGMENT.append(
                gen_load_instruction(Instruction.LOAD_ADDR.value,
                                     REGS[index],
                                     val))
        else:
            TEXT_SEGMENT.append(
                gen_load_instruction(Instruction.LOAD_WORD.value,
                                     REGS[index],
                                     val))
        return REGS[index]
    else:
        return None


def is_constant(val):
    t = type(val)
    return t == int


def gen_load_instruction(type, reg, val):
    inst = INSTRUCTIONS[type] + ' '
    inst += '$' + str(reg_index(reg)) + ' ' + str(val)
    return inst


def gen_instructions(q):
    if type(q) == Label:
        return q.lbl

    if q.op == Instruction.WRITE_INT.value:
        inst = INSTRUCTIONS[q.op](get_reg(q.arg1))
    elif q.op == Instruction.WRITE_STRING.value:
        inst = INSTRUCTIONS[q.op](q.arg1[:-1])
    else:
        inst = INSTRUCTIONS[q.op]
        arg1_reg = get_reg(q.arg1)
        arg2_reg = get_reg(q.arg2)
        res_areg = get_reg(q.res)
        #todo
    return inst


def gen_global_sym(sym):
    rv = '.align 3\n'
    if sym.type == str:
        rv += sym.name + ' .asciiz ' + sym.val
    else:
        rv += sym.name + ': .space ' + str(TYPE_SIZE[sym.type.val])
    return rv


def out(f=sys.stdout):
    for line in DATA_SEGMENT:
        print(line, file=f)
    for line in TEXT_SEGMENT:
        print(line, file=f)


def gen(icr):
    for sym in GLOBAL_SYMTAB.values():
        DATA_SEGMENT.append(gen_global_sym(sym))
    for q in icr:
        TEXT_SEGMENT.append(gen_instructions(q))
    TEXT_SEGMENT.append(MAIN_FINI)
    out()
    return None