GLOBAL_SYMTAB = { }


def global_symbols(p):
    if not p:
        return
    GLOBAL_SYMTAB[p.val.name] = p.val
    for decl in p.children:
        GLOBAL_SYMTAB[decl.val.name] = decl.val


def global_string(sym):
    GLOBAL_SYMTAB[sym.name] = sym


def resolve_symbol(sym):
    return GLOBAL_SYMTAB # todo resolve more symbols


def gen(_ast):
    icr = []
    icr += _ast.gen_quad()
    return icr